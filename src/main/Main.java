package main;
import biblioteca.Biblioteca;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Pulsa un bot�n (1-5) para empezar");
		int respuesta=in.nextInt();
		
		if(respuesta==1){
			System.out.println("Introduce el numero a sumar");
			int num1=in.nextInt();
			System.out.println("Introduce el numero a sumar");
			int num2=in.nextInt();
			int suma=Biblioteca.suma(num1, num2);
			System.out.println("La suma es "+suma);
		}
		else if(respuesta==2){
			System.out.println("Introduce el numero a restar");
			int num1=in.nextInt();
			System.out.println("Introduce el numero a restar");
			int num2=in.nextInt();
			int resta=Biblioteca.restar(num1, num2);
			System.out.println("La resta es "+resta);
		}
		else if(respuesta==3){
			System.out.println("Introduce el tama�o de la piramide");
			int num=in.nextInt();
			Biblioteca.piramide(num);
		}
		else if(respuesta==4){
			System.out.println("Introduce el tama�o de la piramide de pares");
			int num=in.nextInt();
			Biblioteca.piramidepares(num);
		}
		else{
			System.exit(0);
		}
	}

}
