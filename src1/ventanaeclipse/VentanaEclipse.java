package ventanaeclipse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JEditorPane;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JSlider;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 661, 513);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		setJMenuBar(menuBar);
		
		JMenuItem mntmFile = new JMenuItem("file");
		menuBar.add(mntmFile);
		
		JMenuItem mntmEdit = new JMenuItem("edit");
		menuBar.add(mntmEdit);
		
		JMenuItem mntmSource = new JMenuItem("source");
		menuBar.add(mntmSource);
		
		JMenuItem mntmHelp = new JMenuItem("help");
		menuBar.add(mntmHelp);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JEditorPane dtrpnVolume = new JEditorPane();
		dtrpnVolume.setText("            Volume:");
		dtrpnVolume.setBackground(Color.RED);
		dtrpnVolume.setForeground(Color.BLACK);
		dtrpnVolume.setBounds(242, 11, 109, 23);
		contentPane.add(dtrpnVolume);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(349, 11, 17, 23);
		contentPane.add(scrollBar);
		
		JButton btnEscribe = new JButton("select");
		btnEscribe.setBackground(Color.BLACK);
		btnEscribe.setForeground(Color.RED);
		btnEscribe.setBounds(10, 40, 81, 26);
		contentPane.add(btnEscribe);
		
		JRadioButton rdbtnCorrecto = new JRadioButton("ON");
		rdbtnCorrecto.setBounds(24, 130, 49, 23);
		contentPane.add(rdbtnCorrecto);
		
		JRadioButton rdbtnFalso = new JRadioButton("OFF");
		rdbtnFalso.setBounds(24, 153, 49, 23);
		contentPane.add(rdbtnFalso);
		
		JCheckBox chckbxAceptoLasCondiciones = new JCheckBox("SPEED");
		chckbxAceptoLasCondiciones.setBounds(569, 163, 76, 23);
		contentPane.add(chckbxAceptoLasCondiciones);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(Color.WHITE);
		comboBox.setForeground(Color.BLUE);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Texto", "Ayuda", "Volumen"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(79, 11, 102, 23);
		contentPane.add(comboBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(213, 45, 29, 20);
		contentPane.add(spinner);
		
		JButton btnValidaContrasea = new JButton("left");
		btnValidaContrasea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnValidaContrasea.setBounds(510, 304, 76, 23);
		contentPane.add(btnValidaContrasea);
		
		JSlider slider = new JSlider();
		slider.setBounds(376, 8, 200, 26);
		contentPane.add(slider);
		
		JButton btnRight = new JButton("right");
		btnRight.setBounds(569, 332, 76, 32);
		contentPane.add(btnRight);
		
		JButton btnStart = new JButton("start");
		btnStart.setBounds(579, 304, 66, 23);
		contentPane.add(btnStart);
		
		JButton btnSave = new JButton("save");
		btnSave.setBounds(579, 40, 66, 26);
		contentPane.add(btnSave);
		
		JButton btnLeft = new JButton("left");
		btnLeft.setBounds(10, 243, 66, 23);
		contentPane.add(btnLeft);
		
		JButton btnRight_1 = new JButton("right");
		btnRight_1.setBounds(61, 243, 76, 23);
		contentPane.add(btnRight_1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Edgar Rubio\\Music\\22dbd27a72dc1362d136bac5c9e27074.png"));
		lblNewLabel.setBounds(10, 0, 635, 439);
		contentPane.add(lblNewLabel);
	}
}
